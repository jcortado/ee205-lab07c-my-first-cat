///////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello1.cpp
/// @version 1.0 - Initial version
///
/// Simple hello world program using namespaces
///
/// @author Jordan Cortado <jcortado@hawaii.edu>
/// 
/// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;


int main() {

   cout << "\nHello World!\n\n" << endl;

   return EXIT_SUCCESS;

}
