///////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello2.cpp
/// @version 1.0 - Initial version
///
/// Simple hello world program without using namespaces
///
/// @author Jordan Cortado <jcortado@hawaii.edu>
///
/// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>


int main () {

   std::cout << "\nHello World!\n\n" << std::endl;

   return EXIT_SUCCESS;
}
