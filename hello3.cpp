///////////////////////////////////////////////////////////////////////////////
///           University of Hawaii, College of Engineering
/// @brief   Lab07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello2.cpp
/// @version 1.0 - Initial version
///
/// Simple hello world program using namespace and a class
///
/// @author Jordan Cortado <jcortado@hawaii.edu>
/// 
/// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;


class Cat {
   public:
      void sayHello() {
         cout << "\nMeow!\n\n" << endl;
      }
};


int main() {

   Cat myCat;

   myCat.sayHello();
}
